﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfInit : IDbInit
    {
        private readonly DataContext _dataContext;

        public EfInit(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitDB()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}
