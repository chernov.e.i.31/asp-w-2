﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(40)]
        public string Code { get; set; }
        [MaxLength(50)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        [MaxLength(100)]
        public string PartnerNameLong { get; set; }

        public Employee PartnerManager { get; set; }

        public Preference Preference { get; set; }
    }
}