﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(20)]
        public string Name { get; set; }
        public IEnumerable<CustomerPreference> Preferences { get; set; }
    }
}