﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }

        public Customer TransformToCustomer()
        {
            return new Customer
            {
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
            };
        }

        public Customer TransformToCustomerExist(Customer customer)
        {
            customer.FirstName= FirstName;
            customer.LastName= LastName;
            customer.Email= Email;
            return customer;
        }
    }
}