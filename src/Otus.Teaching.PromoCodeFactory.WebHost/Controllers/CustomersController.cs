﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            return Ok(customers.Select(x => new CustomerShortResponse(x)).ToList());
        }

        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customers = await _customerRepository.GetByIdAsync(id);
            return Ok(new CustomerResponse(customers));
        }

        /// <summary>
        /// Добавить клиента с предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = request.TransformToCustomer();
            var preferenses = await GetPreferencesAsync(request.PreferenceIds);

            customer.Preferences = preferenses.Select(x => new CustomerPreference { Customer = customer, Preference = x });

            await _customerRepository.AddAsync(customer);
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            return Ok(customer.Id);
        }

        /// <summary>
        /// Обновить клиента с предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer is not Customer)
            {
                return NotFound();
            }
            customer = request.TransformToCustomerExist(customer);

            var preferenses = await GetPreferencesAsync(request.PreferenceIds);

            customer.Preferences = preferenses.Select(x => new CustomerPreference { Customer = customer, Preference = x });

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента и его предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer is not Customer)
            {
                return NotFound();
            }
            await _customerRepository.DeleteAsync(customer);
            return NoContent();
        }

        private async Task<IEnumerable<Preference>> GetPreferencesAsync(IEnumerable<Guid> guids) 
        {
            var elements = guids ?? Enumerable.Empty<Guid>();
            if (elements.Count() > 0)
            {
                return await _preferenceRepository.GetRangeByIdsAsync(elements.ToList());
            }
            return new List<Preference>();
        }
    }
}