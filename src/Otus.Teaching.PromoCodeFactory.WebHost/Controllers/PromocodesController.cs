﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository) 
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            return Ok(promoCodes.Select(x =>
                new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    PartnerName = x.PartnerNameLong,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                    EndDate = x.EndDate.ToString("yyyy-MM-dd")
                }
                ));
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = (await _preferenceRepository.GetAllAsync()).Where(x => x.Name == request.Preference).FirstOrDefault();
            if (preference is not Preference)
            {
                return NotFound();
            }
            var promo = request.TransformToPromoCode();
            promo.Preference = preference;

            await _promoCodeRepository.AddAsync(promo);

            return Ok();
        }
    }
}